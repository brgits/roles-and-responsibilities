Team Members; Antonio Dominguez, Brittany Mitrani

Documentation; Antonio Dominguez (review/approve), Brittany Mitrani (build)
MVC views ; Antonio Dominguez (review/approve), Brittany Mitrani (build)
MVC controller ; Antonio Dominguez (build), Brittany Mitrani (review/approve)
MVC model Antonio; Dominguez (build), Brittany Mitrani (review/approve)